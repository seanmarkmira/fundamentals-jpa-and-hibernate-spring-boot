package com.in28minutes.springboot.learnjpaandhibernate.course;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
import com.in28minutes.springboot.learnjpaandhibernate.course.jdbc.CourseJdbcRepository;
import com.in28minutes.springboot.learnjpaandhibernate.course.jpa.CourseJpaRepository;
import com.in28minutes.springboot.learnjpaandhibernate.course.springdatajpa.CourseSpringDataJpaRepository;

@Component
public class CourseCommandLineRunner implements CommandLineRunner{

//	This is for the SPRING JDBC approach
//	@Autowired
//	private CourseJdbcRepository repository; 
	
//	This is for the Spring JPA approach
//	@Autowired
//	private CourseJpaRepository repository;

	@Autowired
	private CourseSpringDataJpaRepository repository;
	
	@Override
	public void run(String... args) throws Exception {
		
		//This is Spring JDBC and Spring JPA
//		repository.insert(new Course(1, "Learn Principal Engineering", "in28minutes"));
//		repository.insert(new Course(2, "Learn Technical Leadership EngineeringEngineering", "in28minutes"));
//		repository.insert(new Course(3, "Java for Principal Engineers", "in28minutes"));
//		repository.deleteById((long) 1);
//		System.out.println(repository.findById(2));
//		System.out.println(repository.findById(3));
		//This is Spring JDBC and Spring JPA
		
		repository.save(new Course(1, "Learn Principal Engineering", "in28minutes"));
		repository.save(new Course(2, "Learn Technical Leadership EngineeringEngineering", "in28minutes"));
		repository.save(new Course(3, "Java for Principal Engineers", "in28minutes"));
		repository.deleteById((long) 1);
		System.out.println(repository.findById(2l));
		System.out.println(repository.findById(3l));
		
		System.out.println(repository.findAll());
		System.out.println(repository.count());
		System.out.println("findByAuthor");
		System.out.println(repository.findByAuthor("in28minutes"));
		System.out.println("findByAuthor");
		
		System.out.println(repository.findByName("Learn Technical Leadership EngineeringEngineering"));
	}

}
